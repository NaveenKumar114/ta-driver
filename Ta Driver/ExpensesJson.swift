//
//  ExpensesJson.swift
//  Ta Driver
//
//  Created by Naveen Natrajan on 2021-04-27.
//

import Foundation
// MARK: - ExpensesJSON
struct ExpensesJSON: Codable {
    let code: Int?
    let response: String?
    let expense: [Expense]?
}

// MARK: - Expense
struct Expense: Codable {
    let expenseid, jobid, jobtruckid, driverid: String?
    let drivername, chargeType, chargeTypeID, truckNumber: String?
    let truckNumberID, driverVendorID, driverVendorType, userid: String?
    let username, expenseDescription, attachement1, expenseamount: String?
    let typeofexpense, modeofpayment, createdby, modifyby: String?
    let createddate, modifydate, deviceid: String?

    enum CodingKeys: String, CodingKey {
        case expenseid, jobid, jobtruckid, driverid, drivername
        case chargeType = "charge_type"
        case chargeTypeID = "charge_type_id"
        case truckNumber = "truck_number"
        case truckNumberID = "truck_number_id"
        case driverVendorID = "driver_vendor_id"
        case driverVendorType = "driver_vendor_type"
        case userid, username
        case expenseDescription = "description"
        case attachement1, expenseamount, typeofexpense, modeofpayment, createdby, modifyby, createddate, modifydate, deviceid
    }
}

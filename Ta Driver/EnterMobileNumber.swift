//
//  EnterMobileNumber.swift
//  Ta Driver
//
//  Created by Naveen Natrajan on 2021-04-29.
//

import FirebaseAuth
import Firebase
import UIKit
import CountryPickerView
class EnterMobileNumber: UIViewController, CountryPickerViewDelegate {
    var countryCode : String?
    func countryPickerView(_ countryPickerView: CountryPickerView, didSelectCountry country: Country) {
        print(country.phoneCode)
        countryCode = country.phoneCode
    }
    
    @IBOutlet weak var mobileNumberTextField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        let cpv = CountryPickerView(frame: CGRect(x: 0, y: 0, width: 120, height: 20))
        mobileNumberTextField.leftView = cpv
        mobileNumberTextField.leftViewMode = .always
        countryCode = cpv.selectedCountry.phoneCode
        cpv.delegate = self
        self.navigationController?.isNavigationBarHidden = false
        // Do any additional setup after loading the view.
    }
    
    @IBAction func sendButton(_ sender: Any) {
        if mobileNumberTextField.text != nil
        {
            sendotp()
        }
    }
    
    func sendotp()
    {
        Auth.auth().languageCode = "en"
        let phoneNumber = "\(countryCode!)\(mobileNumberTextField.text!)"
        print(phoneNumber)
        PhoneAuthProvider.provider().verifyPhoneNumber(phoneNumber, uiDelegate: nil) { [self] (verificationID, error) in
          if let error = error {
            print(error.localizedDescription)

            //self.showMessagePrompt(error.localizedDescription)
            return
          }
            UserDefaults.standard.set(verificationID, forKey: "authVerificationID")
            UserDefaults.standard.set("\(mobileNumberTextField.text!)", forKey: "mobile")

            self.performSegue(withIdentifier: "otp", sender: nil)

          // Sign in using the verificationID and the code sent to the user
          // ...
        }
    }

}

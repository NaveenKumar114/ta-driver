//
//  Login.swift
//  Ta Driver
//
//  Created by Naveen Natrajan on 2021-04-22.
//

import UIKit
import SystemConfiguration
import CoreTelephony
import Firebase
import FirebaseAuth
class Login: UIViewController, UITextFieldDelegate {
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
      
        self.navigationController?.navigationBar.isTranslucent = false
   
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default) //UIImage.init(named: "transparent.png")
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
      //  self.navigationController?.isNavigationBarHidden = true

    }
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var password: UITextField!
    var tokenID = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        email.text = "9941269697"
        password.isSecureTextEntry = true
        password.text = "123456"
        email.delegate = self
        password.delegate = self
        Messaging.messaging().token { token, error in
          if let error = error {
            print("Error fetching FCM registration token: \(error)")
          } else if let token = token {
            print("FCM registration token: \(token)")
            self.tokenID = token
          //  self.fcmRegTokenMessage.text  = "Remote FCM registration token: \(token)"
          }
        }
       // makePostCalltest()
        self.navigationController?.isNavigationBarHidden = true
        // Do any additional setup after loading the view.
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
     
        return false

    }
    func makePostCalltest() {
       // let phone = UserDefaults.standard.string(forKey: "mobile")
        let decoder = JSONDecoder()
       
        let postString = "driver_email=7904176492&tokenid="
        print(postString)
        // create post request
        if let url = URL(string: "\(ConstantsUsedInProject.baseUrl)driver/iosdriverloginbymobile")
        {
            let request = NSMutableURLRequest(url: url)
            request.httpMethod = "POST"
            request.setValue("application/x-www-form-urlencoded; charset=utf-8", forHTTPHeaderField: "Content-Type")
            request.httpBody = postString.data(using: String.Encoding.utf8)
            let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
                guard error == nil && data != nil else {                                                          // check for fundamental networking error
                    print("error=\(String(describing: error))")
                    
                    return
                }
                do {
                     print(String(data: data!, encoding: String.Encoding.utf8) as Any)

                    let loginBaseResponse = try? decoder.decode(EmailJson.self, from: data!)
                    let code_str = loginBaseResponse!.code
                    
                    DispatchQueue.main.async {
                        
                        if code_str == 200 {
                            
                            let defaults = UserDefaults.standard
                            defaults.setValue(loginBaseResponse?.driver?.driverIDPrimary ?? "", forKey: "id") // this is the driver id used
                            defaults.setValue(loginBaseResponse?.driver?.driverName ?? "", forKey: "name")
                            defaults.setValue(loginBaseResponse?.driver?.driverLicenseFile ?? "", forKey: "licenceimg")
                            defaults.setValue(loginBaseResponse?.driver?.profileImage ?? "", forKey: "img")

                            defaults.setValue(loginBaseResponse?.driver?.driverAddress ?? "", forKey: "address")
                            defaults.setValue(loginBaseResponse?.driver?.driverMobile ?? "", forKey: "mobile")


                            defaults.setValue("false", forKey: "\(userdefaultsKey.jobSelected)")
                            defaults.setValue("TRUE", forKey: "loggedIn")

                            print(loginBaseResponse as Any)
                            let storyboard2 = UIStoryboard(name: "Dashboard", bundle: nil)

                            let dashboard = storyboard2.instantiateViewController(identifier: "dash")

                            (UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate)?.changeRootViewController(dashboard)
                            
                        }else if code_str == 201  {
                            //print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                            
                            
                            let alert = UIAlertController(title: "Login", message: "Invalid Email & Password", preferredStyle: UIAlertController.Style.alert)
                            
                            // add an action (button)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                            
                            // show the alert
                            self.present(alert, animated: true, completion: nil)
                            
                            
                        }
                        
                    }
                    
                    
                }
            }
            task.resume()
        }
    }

    @IBAction func passwordSwith(_ sender: Any) {
        let x = sender as! UISwitch
        if x.isOn
        {
            password.isSecureTextEntry = false
        }
        else
        {
            password.isSecureTextEntry = true
        }
    }
    @IBAction func loginButtonPressed(_ sender: Any) {
makePostCall()
    }
    func getConnectionType() -> String {
        guard let reachability = SCNetworkReachabilityCreateWithName(kCFAllocatorDefault, "www.google.com") else {
            return "NO INTERNET"
        }
        
        var flags = SCNetworkReachabilityFlags()
        SCNetworkReachabilityGetFlags(reachability, &flags)
        
        let isReachable = flags.contains(.reachable)
        let isWWAN = flags.contains(.isWWAN)
        
        if isReachable {
            if isWWAN {
                let networkInfo = CTTelephonyNetworkInfo()
                let carrierType = networkInfo.serviceCurrentRadioAccessTechnology
                
                guard let carrierTypeName = carrierType?.first?.value else {
                    return "UNKNOWN"
                }
                
                switch carrierTypeName {
                case CTRadioAccessTechnologyGPRS, CTRadioAccessTechnologyEdge, CTRadioAccessTechnologyCDMA1x:
                    return "2G"
                case CTRadioAccessTechnologyLTE:
                    return "4G"
                default:
                    return "3G"
                }
            } else {
                return "WIFI"
            }
        } else {
            return "NO INTERNET"
        }
    }
    func makePostCall() {
        UIDevice.current.isBatteryMonitoringEnabled = true
        print(UIDevice.current.isBatteryMonitoringEnabled)
        var level = UIDevice.current.batteryLevel
        print(level*100)
        if level == -1.0
        {
            level = 0.00
        }
        let version = UIDevice.current.systemVersion
        print(version)
        let name = UIDevice.current.name
        print(name)
        let model = UIDevice.current.model
        print(model)
        let state = UIDevice.current.batteryState
        var batteryType = "discharging"
        switch state {
        case .charging:
            print("charging")
        case .full:
            batteryType = "full"
            
        case .unplugged:
            batteryType = "discharging"
            
        default:
            print("else")
        }
        print(getConnectionType())
        let networkInfo = CTTelephonyNetworkInfo()
        let carrier = networkInfo.subscriberCellularProvider
        
        // Get carrier name
        var carrierName = carrier?.carrierName
        if carrierName == nil
        {
            carrierName = ""
        }
       
        let id = UserDefaults.standard.string(forKey: "id")
        let decoder = JSONDecoder()
       
        let postString = "driver_email=\(email.text!)&driver_password=\(password.text!)&tokenid=0&battery_type=\(batteryType)&battery_percentage=\(Int(level*100))&network_type=\(getConnectionType())&network_operator=\(carrierName ?? "")&device_name=\(name)&device_manufracture=Apple&device_brand=\(model)&device_os=\(version)&device_model=\(model)"
        // create post request
        if let url = URL(string: "\(ConstantsUsedInProject.baseUrl)driver/iosdriverlogin")
        {
            let request = NSMutableURLRequest(url: url)
            request.httpMethod = "POST"
            request.setValue("application/x-www-form-urlencoded; charset=utf-8", forHTTPHeaderField: "Content-Type")
            request.httpBody = postString.data(using: String.Encoding.utf8)
            let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
                guard error == nil && data != nil else {                                                          // check for fundamental networking error
                    print("error=\(String(describing: error))")
                    
                    return
                }
                do {
                    
                    let loginBaseResponse = try? decoder.decode(EmailJson.self, from: data!)
                    let code_str = loginBaseResponse!.code
                    // print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                    
                    DispatchQueue.main.async {
                        
                        if code_str == 200 {
                            
                            let defaults = UserDefaults.standard
                            defaults.setValue(loginBaseResponse?.driver?.driverIDPrimary ?? "", forKey: "id") // this is the driver id used
                            defaults.setValue(loginBaseResponse?.driver?.driverName ?? "", forKey: "name")
                            defaults.setValue(loginBaseResponse?.driver?.driverLicenseFile ?? "", forKey: "licenceimg")
                            defaults.setValue(loginBaseResponse?.driver?.profileImage ?? "", forKey: "img")

                            defaults.setValue(loginBaseResponse?.driver?.driverAddress ?? "", forKey: "address")
                            defaults.setValue(loginBaseResponse?.driver?.driverMobile ?? "", forKey: "mobile")


                            defaults.setValue("false", forKey: "\(userdefaultsKey.jobSelected)")
                            defaults.setValue("TRUE", forKey: "loggedIn")

                            print(loginBaseResponse as Any)
                            let storyboard2 = UIStoryboard(name: "Dashboard", bundle: nil)

                            let dashboard = storyboard2.instantiateViewController(identifier: "dash")

                            (UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate)?.changeRootViewController(dashboard)
                            
                        }else if code_str == 201  {
                            //print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                            
                            
                            let alert = UIAlertController(title: "Login", message: "Invalid Email & Password", preferredStyle: UIAlertController.Style.alert)
                            
                            // add an action (button)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                            
                            // show the alert
                            self.present(alert, animated: true, completion: nil)
                            
                            
                        }
                        
                    }
                    
                    
                }
            }
            task.resume()
        }
    }


}

//
//  Expenses.swift
//  Ta Driver
//
//  Created by Naveen Natrajan on 2021-04-27.
//

import UIKit

class Expenses: UIViewController, UITableViewDelegate, UITableViewDataSource , addExpenseProtocol {
    func refresh() {
        makeGetCall()
    }
    
    @IBOutlet weak var payLabel: UILabel!
    var jobData : Datum?
    var expenseData : ExpensesJSON?
    @IBOutlet weak var allowance: UILabel!
    @IBOutlet weak var expenseTableView: UITableView!
    @IBOutlet weak var expense: UILabel!
    @IBOutlet weak var advance: UILabel!
    var totalExpense : Double = 0.0
    var editExpense : Int?
    override func viewDidLoad() {
        super.viewDidLoad()
        payLabel.text = ""
        expenseTableView.delegate = self
        expenseTableView.dataSource = self
        expenseTableView.register(UINib(nibName: "ExpenseListCell", bundle: nil) , forCellReuseIdentifier: "ExpenseListCell")
        makeGetCall()
        self.title = "\(jobData?.jobNumber ?? "") - EXPENSE"
        let x = jobData!
        var y = Double(x.overnightTotalAmount ?? "0.0")
        var s = String(format: "%.2f", y ?? 0.0)

        advance.text = ":MYR \(s)"
         y = Double(x.totalCommision ?? "0.0")
         s = String(format: "%.2f", y ?? 0.0)

        allowance.text = ":MYR \(s)"

    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  expenseData?.expense?.count ?? 0
    }
    
    @IBAction func addExpenseClicked(_ sender: Any) {
        performSegue(withIdentifier: "toAddExpense", sender: nil)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toAddExpense"
        {
            let vc = segue.destination as! AddExpense
            vc.jobData = jobData
            vc.delegate = self
            if editExpense != nil
            {
                vc.editData = expenseData?.expense?[editExpense!]
                editExpense = nil
            }
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = expenseTableView.dequeueReusableCell(withIdentifier: "ExpenseListCell") as! ExpenseListCell
      if  let x = expenseData?.expense?[indexPath.row]
      {
        let d = convertDateFormater(x.modifydate!)
        cell.date.text = d.0
        cell.time.text = d.1
        cell.name.text = x.drivername ?? ""
        cell.type.text = x.typeofexpense ?? ""
        cell.circleVIew.layer.cornerRadius = cell.circleVIew.frame.width / 2
        let y = Double(x.expenseamount ?? "0.0")
        let s = String(format: "%.2f", y ?? 0.0)
        cell.amount.text = "MYR: \(s)"
        cell.mode.text = "MODE: \(x.modeofpayment ?? "")"
        if x.attachement1 != nil
        {
            let urlStr = ("\(ConstantsUsedInProject.baseImgUrl)expense/\(x.attachement1!)")
            print(urlStr)
            let url = URL(string: urlStr)
            
            DispatchQueue.global().async {
                let data = try? Data(contentsOf: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
                DispatchQueue.main.async { [self] in
                    cell.expenseImage.image = UIImage(data: data!)
                   // logoImageVIew.image = UIImage(data: data!)
                  //  logoImageVIew.contentMode = .scaleAspectFit
                    
                  
                }
            }
        }

      }
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 85
    }
    func tableView(_ tableView: UITableView,trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration?
         {
       //  action.backgroundColor = .yellow
         let actionApply = UIContextualAction(style: .normal, title:  "Update", handler: { (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
                 print("Apply")
             success(true)

            self.editExpense = indexPath.row
            self.performSegue(withIdentifier: "toAddExpense", sender: nil)

                             
             })
  

     actionApply.backgroundColor = .green
   //  actionApply.image = UIImage(systemName: "hand.tap.fill")

             return UISwipeActionsConfiguration(actions: [actionApply])
         }
    func makeGetCall() {
        let id = UserDefaults.standard.string(forKey: "id")
        let decoder = JSONDecoder()
        let request = NSMutableURLRequest(url: NSURL(string: "\(ConstantsUsedInProject.baseUrl)expense/\(jobData!.jobID!)/\(id!)")! as URL)
        request.httpMethod = "GET"
        request.setValue("application/x-www-form-urlencoded; charset=utf-8", forHTTPHeaderField: "Content-Type")
        let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
            guard error == nil && data != nil else {                                                          // check for fundamental networking error
                print("error=\(String(describing: error))")
                return
            }
            do {
                let responseJ = try? decoder.decode(ExpensesJSON.self, from: data!)
                let code_str = responseJ?.code
                DispatchQueue.main.async { [self] in
                    if code_str == 200 {
                        print("success")
                        print(responseJ as Any)
                       // self.trackData = responseJ
                        self.expenseData = responseJ
                        totalExpense = 0.0
                        self.expenseTableView.reloadData()
                        for i in expenseData!.expense! {
                            let y = Double(i.expenseamount! )
                            totalExpense = y! + totalExpense
                        }
                        let z = String(format: "%.2f", totalExpense)
                        expense.text = ":MYR \(z)"
                        payLabel.text = "YOU WILL COLLECT -\(z) FROM OFFICE"
                        
                        //self.tackTableView.reloadData()
                    }else if code_str == 201  {
                        let alert = UIAlertController(title: "Track", message: "\(responseJ?.response ?? "error")", preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    }
                }
            }
        }
        task.resume()
    }
    func convertDateFormater(_ date: String) -> (String , String)
        {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let date = dateFormatter.date(from: date)
        dateFormatter.dateFormat = "h:mm a"
            let t = dateFormatter.string(from: date!)

            dateFormatter.dateFormat = "dd-MM-yyyy"
            return  (dateFormatter.string(from: date!) , t)

        }
}

//
//  JobDepartureDeliveryJson.swift
//  Ta Driver
//
//  Created by Naveen Natrajan on 2021-04-24.
//

import Foundation

// MARK: - JobDepartureDeliveryJSON
struct JobDepartureDeliveryJSON: Codable {
    let code: Int?
    let response: String?
    let tracking: [Tracking]?
}

// MARK: - Tracking
struct Tracking: Codable {
    let trackid, jobid, jobtruckid, clientid: String?
    let driverid, drivername, jobstatus, trackingDescription: String?
    let trackdatetime, currentlocation, pickuplocation, destinationlocation: String?
    let latitude, longitude, bearing, bearingaccuracydegrees: String?
    let speed, speedaccuracymeterspersecond, accuracy, status: String?
    let jobnumber, createby, modifyby, createdate: String?
    let modifydate, deviceid, signimage, signEmpName: String?
    let signEmpID, attachmentimage, geofence: String?

    enum CodingKeys: String, CodingKey {
        case trackid, jobid, jobtruckid, clientid, driverid, drivername, jobstatus
        case trackingDescription = "description"
        case trackdatetime, currentlocation, pickuplocation, destinationlocation, latitude, longitude, bearing, bearingaccuracydegrees, speed, speedaccuracymeterspersecond, accuracy, status, jobnumber, createby, modifyby, createdate, modifydate, deviceid, signimage
        case signEmpName = "sign_emp_name"
        case signEmpID = "sign_emp_id"
        case attachmentimage, geofence
    }
}

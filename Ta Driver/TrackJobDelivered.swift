//
//  TrackJobDelivered.swift
//  Ta Driver
//
//  Created by Naveen Natrajan on 2021-04-28.
//

import UIKit

class TrackJobDelivered: UIViewController , UITableViewDelegate, UITableViewDataSource{

    @IBOutlet weak var status: UILabel!
    @IBOutlet weak var client: UILabel!
    @IBOutlet weak var from: UILabel!
    @IBOutlet weak var to: UILabel!
    @IBOutlet weak var allowance: UILabel!
    @IBOutlet weak var advance: UILabel!
    
    @IBOutlet weak var date: UILabel!
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var x = 0
        if trackData != nil
        {
            x = trackData?.trackList?.count ?? 0
            x = x + 1
            print(x)
            print(trackData?.trackList?.count)
        }
       
        return x
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == trackData?.trackList?.count
        {
            let cell = tackTableView.dequeueReusableCell(withIdentifier: "TrackJobImages") as! TrackJobImages
            let i = (trackData?.trackList!.count)!
            if let x = trackData?.trackList?[0]
            {
                cell.id.text = x.signEmpID ?? ""
                cell.name.text = x.signEmpName ?? ""
                
                if x.signimage != nil
                {
                    let urlStr = ("\(ConstantsUsedInProject.baseImgUrl)track/\(x.signimage!)")
                    print(urlStr)
                    let url = URL(string: urlStr)
                    
                    DispatchQueue.global().async {
                        let data = try? Data(contentsOf: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
                        DispatchQueue.main.async { [self] in
                            cell.signature.image = UIImage(data: data!)
                           // logoImageVIew.image = UIImage(data: data!)
                          //  logoImageVIew.contentMode = .scaleAspectFit
                            
                          
                        }
                    }
                }
                for i in trackData!.trackList!
                {
                    if i.jobstatus! == "DEPARTURE"
                    {
                        if x.attachmentimage != nil
                        {
                            let urlStr = ("\(ConstantsUsedInProject.baseImgUrl)track/\(i.attachmentimage!)")
                            print(urlStr)
                            let url = URL(string: urlStr)
                            
                            DispatchQueue.global().async {
                                let data = try? Data(contentsOf: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
                                DispatchQueue.main.async { [self] in
                                    cell.PICKUP.image = UIImage(data: data!)
                                   // logoImageVIew.image = UIImage(data: data!)
                                  //  logoImageVIew.contentMode = .scaleAspectFit
                                    
                                  
                                }
                            }
                        }

                    }
                }
                                if x.attachmentimage != nil
                {
                    let urlStr = ("\(ConstantsUsedInProject.baseImgUrl)track/\(x.attachmentimage!)")
                    print(urlStr)
                    let url = URL(string: urlStr)
                    
                    DispatchQueue.global().async {
                        let data = try? Data(contentsOf: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
                        DispatchQueue.main.async { [self] in
                            cell.delivery.image = UIImage(data: data!)
                           // logoImageVIew.image = UIImage(data: data!)
                          //  logoImageVIew.contentMode = .scaleAspectFit
                            
                          
                        }
                    }
                }
            }
            return cell
        }
        else
        {
        let cell = tackTableView.dequeueReusableCell(withIdentifier: "JobListCell") as! JobListCell
        let i = ((trackData?.trackList!.count)! - 1) - indexPath.row
        if let x = trackData?.trackList?[i]
        {
           // cell.address.text = x.pickuplocation ?? ""
            let d = convertDateFormater(x.trackdatetime!)
            cell.date.text = d.0
            cell.time.text = d.1

            cell.status.text = x.jobstatus ?? ""
            cell.circleVIew.layer.cornerRadius = cell.circleVIew.frame.width / 2
            if indexPath.row % 2 == 0
            {
                cell.circleVIew.backgroundColor = #colorLiteral(red: 0.6700325012, green: 0.1703311503, blue: 0.1363497078, alpha: 1)
                
            }
            else
            {
                cell.circleVIew.backgroundColor = #colorLiteral(red: 0.1349480152, green: 0.383015275, blue: 0.5483094454, alpha: 1)
            }
            switch x.jobstatus!  {
            case "ASSIGNED":
               // cell.iconImage.image = UIImage(named: "")?.withTintColor(.white)
            print("ee")
            case "COLLECTING":
                cell.iconImage.image = UIImage(named: "black_collecting")?.withTintColor(.white)
            case "PICKED UP":
                cell.iconImage.image = UIImage(named: "black_pickedup2")?.withTintColor(.white)
            case "DEPARTURE":
                cell.iconImage.image = UIImage(named: "black_departure")?.withTintColor(.white)
            case "IN TRANSIT":
                cell.iconImage.image = UIImage(named: "black_transit")?.withTintColor(.white)
            case "DELIVERED":
                cell.iconImage.image = UIImage(named: "black_delivered")?.withTintColor(.white)
            default:
                print("err")
            }

            
        }
        return cell
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == trackData?.trackList?.count

        {
         return 100
        }
        else
        {
        return 70
        }
    }
    func convertDateFormater(_ date: String) -> (String , String)
        {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let date = dateFormatter.date(from: date)
        dateFormatter.dateFormat = "h:mm a"
            let t = dateFormatter.string(from: date!)

            dateFormatter.dateFormat = "dd-MM-yyyy"
            return  (dateFormatter.string(from: date!) , t)

        }

    var jobData : Datum?
    var trackData : TrackListJSON?
    @IBOutlet weak var tackTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
makeGetCall()
        tackTableView.delegate = self
        tackTableView.dataSource = self
        tackTableView.register(UINib(nibName: "JobListCell", bundle: nil) , forCellReuseIdentifier: "JobListCell")
        
        tackTableView.register(UINib(nibName: "TrackJobImages", bundle: nil) , forCellReuseIdentifier: "TrackJobImages")

        var y = Double(jobData?.overnightTotalAmount ?? "0.0")
        var s = String(format: "%.2f", y ?? 0.0)

        advance.text = "MYR \(s)"
        y = Double(jobData?.totalCommision ?? "0.0")
         s = String(format: "%.2f", y ?? 0.0)

        allowance.text = "MYR \(s)"
        from.text = jobData?.jobFrom ?? ""
        to.text = jobData?.jobTo ?? ""
        status.text = jobData?.jobStatus ?? ""
        date.text = jobData?.jobDate ?? ""
        client.text = jobData?.jobClientName?.uppercased() ?? ""
        self.title = jobData?.jobNumber ?? ""
        // Do any additional setup after loading the view.
    }
    
    func makeGetCall() {
        let decoder = JSONDecoder()
        let request = NSMutableURLRequest(url: NSURL(string: "\(ConstantsUsedInProject.baseUrl)track/index_get/\(jobData!.taFormID!)/\(jobData!.jobID!)/2021-04-26")! as URL)
        request.httpMethod = "GET"
        request.setValue("application/x-www-form-urlencoded; charset=utf-8", forHTTPHeaderField: "Content-Type")
        let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
            guard error == nil && data != nil else {                                                          // check for fundamental networking error
                print("error=\(String(describing: error))")
                return
            }
            do {
                let responseJ = try? decoder.decode(TrackListJSON.self, from: data!)
                let code_str = responseJ?.code
                DispatchQueue.main.async {
                    if code_str == 200 {
                        print("success")
                        print(responseJ as Any)
                        self.trackData = responseJ
                        self.tackTableView.reloadData()
                    }else if code_str == 201  {
                        let alert = UIAlertController(title: "Track", message: "\(responseJ?.response ?? "error")", preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    }
                }
            }
        }
        task.resume()
    }
  

}

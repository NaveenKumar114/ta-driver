//
//  TrackListJson.swift
//  Ta Driver
//
//  Created by Naveen Natrajan on 2021-04-26.
//

import Foundation
import Foundation

// MARK: - TrackListJSON
struct TrackListJSON: Codable {
    let code: Int?
    let response: String?
    let trackList: [TrackList]?
}

// MARK: - TrackList
struct TrackList: Codable {
    let trackid, signEmpID, signEmpName, latitude: String?
    let longitude, jobid, jobtruckid, driverid: String?
    let drivername, jobstatus, createby, status: String?
    let bearing, speed, accuracy, signimage: String?
    let attachmentimage, destinationlocation, pickuplocation, trackdatetime: String?

    enum CodingKeys: String, CodingKey {
        case trackid
        case signEmpID = "sign_emp_id"
        case signEmpName = "sign_emp_name"
        case latitude, longitude, jobid, jobtruckid, driverid, drivername, jobstatus, createby, status, bearing, speed, accuracy, signimage, attachmentimage, destinationlocation, pickuplocation, trackdatetime
    }
}

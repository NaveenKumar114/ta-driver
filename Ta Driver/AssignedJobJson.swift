//
//  AssignedJobJson.swift
//  Ta Driver
//
//  Created by Naveen Natrajan on 2021-04-22.
//



import Foundation
struct AssignedJobJSON: Codable {
    let code: Int?
    let response: String?
    let data: [Datum]?
}

// MARK: - Datum
struct Datum: Codable {
    let jobID, jobNumber, jobDate, jobClientName: String?
    let jobFrom, jobTo, jobChargeType, truckNumberid: String?
    let truckNumber, jobCommision, jobRemark, overnightNotes: String?
    let overnightFromdate, overnightTodate, overnightType, overnightAmount: String?
    let overnightDays, overnightTotalAmount, jobStatus, status: String?
    let currentposition, taFormID, loginID, jobCreatedDate: String?
    let jobModifyDate, jobCreatedBy, jobModifyBy, sequence: String?
    let taStatus, totalCommision, totalAdvance, totalBalance: String?
    let totalGivenbydriver, totalPaytodriver, totalCashSpend, sumOfJobCommision: String?
    let sumOfCashExpense, sumOfCardExpense, totalJobCommission, nightDutyCommision: String?
    let addtionalPickupCommsion, totalJobCommissionRemark, nightDutyCommisionRemark, addtionalPickupCommsionRemark: String?
    let taDriverName: String?
    let taDriverID, taTadynamicNumber, taFormdate, operationStatus: String?
       let accountStatus, managerStatus, managerStatusBy: String?
       let managerStatusTime: String?
       let operationStatusBy, operationStatusTime, accountStatusBy: String?
       let accountStatusTime: String?
       let operationDuration, accountDuration, managerDuration, rejectedTaID: String?
       let rejectedTaNumber, rejectedComment, rejectFlag, requestComment: String?
       let cancelComment, rejectedBy, rejectLoginID: String?
       let requestType: String?
       let isOpenFlag, openBy, openByID, createddate: String?
       let modifydate, createby, modifyby: String?
    let billtoID, billtoBranchname, billtoBranchid, billtoContactperson: String?
     let billtoMobile, billtoLatitude, billtoLongitude, billtoLocationaddress: String?
     let shipperID, shipperBranchname, shipperBranchid, shipperContactperson: String?
     let shipperMobile, shipperLatitude, shipperLongitude, shipperLocationaddress: String?
     let consigneeID, consigneeBranchname, consigneeBranchid, consigneeContactperson: String?
     let consigneeMobile, consigneeLatitude, consigneeLongitude, consigneeLocationaddress: String?
    
    enum CodingKeys: String, CodingKey {
        case jobID = "job_id"
        case jobNumber = "job_number"
        case jobDate = "job_date"
        case jobClientName = "job_client_name"
        case jobFrom = "job_from"
        case jobTo = "job_to"
        case jobChargeType = "job_charge_type"
        case truckNumberid = "truck_numberid"
        case truckNumber = "truck_number"
        case jobCommision = "job_commision"
        case jobRemark = "job_remark"
        case overnightNotes = "overnight_notes"
        case overnightFromdate = "overnight_fromdate"
        case overnightTodate = "overnight_todate"
        case overnightType = "overnight_type"
        case overnightAmount = "overnight_amount"
        case overnightDays = "overnight_days"
        case overnightTotalAmount = "overnight_total_amount"
        case jobStatus = "job_status"
        case status, currentposition
        case taFormID = "ta_form_id"
        case loginID = "login_id"
        case jobCreatedDate = "job_created_date"
        case jobModifyDate = "job_modify_date"
        case jobCreatedBy = "job_created_by"
        case jobModifyBy = "job_modify_by"
        case sequence
        case taStatus = "ta_status"
        case totalCommision = "total_commision"
        case totalAdvance = "total_advance"
        case totalBalance = "total_balance"
        case totalGivenbydriver = "total_givenbydriver"
        case totalPaytodriver = "total_paytodriver"
        case totalCashSpend = "total_cash_spend"
        case sumOfJobCommision = "sum_of_job_commision"
        case sumOfCashExpense = "sum_of_cash_expense"
        case sumOfCardExpense = "sum_of_card_expense"
        case totalJobCommission = "total_job_commission"
        case nightDutyCommision = "night_duty_commision"
        case addtionalPickupCommsion = "addtional_pickup_commsion"
        case totalJobCommissionRemark = "total_job_commission_remark"
        case nightDutyCommisionRemark = "night_duty_commision_remark"
        case addtionalPickupCommsionRemark = "addtional_pickup_commsion_remark"
        case taDriverName = "ta_driver_name"
        case taDriverID = "ta_driver_id"
              case taTadynamicNumber = "ta_tadynamic_number"
              case taFormdate = "ta_formdate"
              case operationStatus = "operation_status"
              case accountStatus = "account_status"
              case managerStatus = "manager_status"
              case managerStatusBy = "manager_status_by"
              case managerStatusTime = "manager_status_time"
              case operationStatusBy = "operation_status_by"
              case operationStatusTime = "operation_status_time"
              case accountStatusBy = "account_status_by"
              case accountStatusTime = "account_status_time"
              case operationDuration = "operation_duration"
              case accountDuration = "account_duration"
              case managerDuration = "manager_duration"
              case rejectedTaID = "rejected_ta_id"
              case rejectedTaNumber = "rejected_ta_number"
              case rejectedComment = "rejected_comment"
              case rejectFlag = "reject_flag"
              case requestComment = "request_comment"
              case cancelComment = "cancel_comment"
              case rejectedBy = "rejected_by"
              case rejectLoginID = "reject_login_id"
              case requestType = "request_type"
              case isOpenFlag = "is_open_flag"
              case openBy = "open_by"
              case openByID = "open_by_id"
        case billtoID = "billto_id"
               case billtoBranchname = "billto_branchname"
               case billtoBranchid = "billto_branchid"
               case billtoContactperson = "billto_contactperson"
               case billtoMobile = "billto_mobile"
               case billtoLatitude = "billto_latitude"
               case billtoLongitude = "billto_longitude"
               case billtoLocationaddress = "billto_locationaddress"
               case shipperID = "shipper_id"
               case shipperBranchname = "shipper_branchname"
               case shipperBranchid = "shipper_branchid"
               case shipperContactperson = "shipper_contactperson"
               case shipperMobile = "shipper_mobile"
               case shipperLatitude = "shipper_latitude"
               case shipperLongitude = "shipper_longitude"
               case shipperLocationaddress = "shipper_locationaddress"
               case consigneeID = "consignee_id"
               case consigneeBranchname = "consignee_branchname"
               case consigneeBranchid = "consignee_branchid"
               case consigneeContactperson = "consignee_contactperson"
               case consigneeMobile = "consignee_mobile"
               case consigneeLatitude = "consignee_latitude"
               case consigneeLongitude = "consignee_longitude"
               case consigneeLocationaddress = "consignee_locationaddress"
              case createddate, modifydate, createby, modifyby
    }
}

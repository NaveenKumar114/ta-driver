//
//  ConstantsUsedInProject.swift
//  Ta Driver
//
//  Created by Naveen Natrajan on 2021-04-23.
//

import Foundation
import UIKit
struct ConstantsUsedInProject {
    static let appThemeColor : UIColor = #colorLiteral(red: 0.9219180346, green: 0.3579308987, blue: 0.2009037137, alpha: 1)
    static let appThemeColorName = "AccentColor"
    static let baseUrl = "https://glowfreight.com.my/taform_demo/taformapi/driverapi/api/"
    static let baseImgUrl = "https://glowfreight.com.my/taform_demo/uploads/"
}
struct userdefaultsKey {
    static let jobid = "jobid"
    static let userID = "userid"
    static let truckid = "truckid"
    static let cliendid = "clientid"
    static let driverID = "id" // driver id is id got during login
    static let jobStatus = "jobstatus"
    static let createBy = "createBy"
    static let name = "name"
    static let jobNumber = "jobnumer"
    static let jobSelected = "jobselected"
}

//
//  ProfileUpdate.swift
//  Ta Driver
//
//  Created by Naveen Natrajan on 2021-04-28.
//

import UIKit
import Alamofire
class ProfileUpdate: UIViewController, UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    @IBOutlet weak var licenceImageView: UIImageView!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var name: UITextField!
    @IBOutlet weak var phoneNumber: UITextField!
    @IBOutlet weak var address: UITextField!
    var profile : UIImage?
    var licence : UIImage?
    var currentImageViewSelected = 0 //1 is profile , 2 is licence imageview
    override func viewDidLoad() {
        super.viewDidLoad()
        name.delegate = self
        phoneNumber.delegate = self
        address.delegate = self
        let defaults = UserDefaults.standard
        name.text = defaults.string(forKey: "name")
        phoneNumber.text = defaults.string(forKey: "mobile")
        address.text = defaults.string(forKey: "address")
        if defaults.string(forKey: "img") != nil
        {
            let urlStr = ("\(ConstantsUsedInProject.baseImgUrl)driver/profile/\(defaults.string(forKey: "img")!)")
            print(urlStr)
            let url = URL(string: urlStr)
            
            DispatchQueue.global().async {
                let data = try? Data(contentsOf: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
                DispatchQueue.main.async { [self] in
                    profileImageView.image = UIImage(data: data!)
                    //logoImageVIew.contentMode = .scaleAspectFit
                    
                   
                }
            }
        }
        if defaults.string(forKey: "licenceimg") != nil
        {
            let urlStr = ("\(ConstantsUsedInProject.baseImgUrl)driver/profile/\(defaults.string(forKey: "licenceimg")!)")
            print(urlStr)
            let url = URL(string: urlStr)
            
            DispatchQueue.global().async {
                let data = try? Data(contentsOf: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
                DispatchQueue.main.async { [self] in
                    licenceImageView.image = UIImage(data: data!)
                    //logoImageVIew.contentMode = .scaleAspectFit
                    
                   
                }
            }
        }
        let gesture1 = UITapGestureRecognizer(target: self, action: #selector(toImage))
        profileImageView.isUserInteractionEnabled = true
        profileImageView.addGestureRecognizer(gesture1)
        profileImageView.tag = 1
        let gesture2 = UITapGestureRecognizer(target: self, action: #selector(toImage))
        licenceImageView.tag = 2
        licenceImageView.isUserInteractionEnabled = true
        licenceImageView.addGestureRecognizer(gesture2)

    }
    @objc func toImage(sender: UITapGestureRecognizer)
    {
        
        let tag = sender.view?.tag
        currentImageViewSelected = tag!
        let chooseImageActionMenu = UIAlertController(title: "Choose an option", message: nil, preferredStyle: .actionSheet)
        let galleryButton = UIAlertAction(title: "Gallery", style: .default) { (_) in
            self.openGallery()
        }
        let cameraButton = UIAlertAction(title: "Camera", style: .default) { (_) in
            self.openCamera()
        }
        let cancelButton = UIAlertAction(title: "Cancel", style: .cancel)
        chooseImageActionMenu.addAction(galleryButton)
        chooseImageActionMenu.addAction(cameraButton)
        chooseImageActionMenu.addAction(cancelButton)
        self.present(chooseImageActionMenu, animated: true, completion: nil)
    }
    @IBAction func updateButtonPressed(_ sender: Any) {
       print("upda")
        let defaults = UserDefaults.standard
        let id = defaults.string(forKey: "id")
        if profile != nil || licence != nil
        {
            if profile == nil && licence != nil
            {
                let json: [String: Any] = ["driver_id_primary" : "\(id!)" , "driver_name": "\(name.text!)" , "driver_phone" : "\(phoneNumber.text!)" , "driver_address" :"\(address.text!)", "upload_type" : "3"]

                let licenceData = licence!.jpegData(compressionQuality: 0.50)
                uploadImageone(image: licenceData!, imageParam: "driver_license", to: URL(string: "\(ConstantsUsedInProject.baseUrl)driver/update_profile")!, params: json)
            }
            if profile != nil && licence == nil
            {
                let json: [String: Any] = ["driver_id_primary" : "\(id!)" , "driver_name": "\(name.text!)" , "driver_phone" : "\(phoneNumber.text!)" , "driver_address" :"\(address.text!)", "upload_type" : "2"]

                let proData = profile!.jpegData(compressionQuality: 0.50)
                uploadImageone(image: proData!, imageParam: "driver_profile", to: URL(string: "\(ConstantsUsedInProject.baseUrl)driver/update_profile")!, params: json)
            }
            if profile != nil && licence != nil
            {
                let json: [String: Any] = ["driver_id_primary" : "\(id!)" , "driver_name": "\(name.text!)" , "driver_phone" : "\(phoneNumber.text!)" , "driver_address" :"\(address.text!)", "upload_type" : "1"]

                let proData = profile!.jpegData(compressionQuality: 0.50)
                let licenceData = licence!.jpegData(compressionQuality: 0.50)

             uploadImageboth(profileimage: proData!, licenceimage: licenceData!, to: URL(string: "\(ConstantsUsedInProject.baseUrl)driver/update_profile")!, params: json)
            }
        }
        else
        {
            let json: [String: Any] = ["driver_id_primary" : "\(id!)" , "driver_name": "\(name.text!)" , "driver_phone" : "\(phoneNumber.text!)" , "driver_address" :"\(address.text!)" , "upload_type" : "0"]

            print("noimage")
         uploadProfileNoImage(to: URL(string: "\(ConstantsUsedInProject.baseUrl)driver/update_profile")!, params: json)
        }
    }
    
    func uploadImageone(image: Data, imageParam : String ,  to url: URL, params: [String: Any]) {
        let block = { (multipart: MultipartFormData) in
            URLEncoding.default.queryParameters(params).forEach { (key, value) in
                /*  if let data = value.data(using: .utf8) {
                      multipart.append(data, withName: key)
                      print(String(data: data, encoding: String.Encoding.utf8) as Any)

                  } */
                  if let data = value.removingPercentEncoding?.data(using: .utf8) {
                     // do your stuff here
                      multipart.append(data, withName: key)
                      //print(String(data: data, encoding: String.Encoding.utf8) as Any)
                  }
                              }
            print(imageParam)
            multipart.append(image, withName: "\(imageParam)", fileName: "profile", mimeType: "image/png")
        }
        
        DispatchQueue.main.async
        {

        
            AF.upload(multipartFormData: block, to: url)
                .uploadProgress(queue: .main, closure: { progress in
                    //Current upload progress of file
                    print("Upload Progress: \(progress.fractionCompleted)")
                })
                .response{
                    response in
                    guard let data = response.data else { return }
                    do {
                        let decoder = JSONDecoder()
                        
                        let loginBaseResponse = try? decoder.decode(EmailJson.self, from: data)
                        
                        
                        let code_str = loginBaseResponse?.code
                        print(String(data: data, encoding: String.Encoding.utf8) as Any)
                        
                        DispatchQueue.main.async { [self] in
                            
                            if code_str == 200 {
                                let defaults = UserDefaults.standard
                                defaults.setValue(loginBaseResponse?.driver?.driverIDPrimary ?? "", forKey: "id") // this is the driver id used
                                defaults.setValue(loginBaseResponse?.driver?.driverName ?? "", forKey: "name")
                                defaults.setValue(loginBaseResponse?.driver?.driverLicenseFile ?? "", forKey: "licenceimg")
                                defaults.setValue(loginBaseResponse?.driver?.profileImage ?? "", forKey: "img")

                                defaults.setValue(loginBaseResponse?.driver?.driverAddress ?? "", forKey: "address")
                                defaults.setValue(loginBaseResponse?.driver?.driverMobile ?? "", forKey: "mobile")
                                print("success")
                                let alert = UIAlertController(title: "Profile", message: "Success", preferredStyle: UIAlertController.Style.alert)
                                
                                // add an action (button)
                                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { (_) in
                                    self.navigationController?.popViewController(animated: true)
                                }))
                                
                                // show the alert
                                self.present(alert, animated: true, completion: nil)
                                
                            }
                            else
                            {
                                let alert = UIAlertController(title: "Profile", message: "\(loginBaseResponse?.response ?? "error")", preferredStyle: UIAlertController.Style.alert)
                                
                                // add an action (button)
                                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                                
                                // show the alert
                                self.present(alert, animated: true, completion: nil)
                            }
                            
                        }
                        
                        
                    }
                }
        }
        
        
    }
    func uploadImageboth(profileimage: Data, licenceimage : Data, to url: URL, params: [String: Any]) {
        let block = { (multipart: MultipartFormData) in
            URLEncoding.default.queryParameters(params).forEach { (key, value) in
                /*  if let data = value.data(using: .utf8) {
                      multipart.append(data, withName: key)
                      print(String(data: data, encoding: String.Encoding.utf8) as Any)

                  } */
                  if let data = value.removingPercentEncoding?.data(using: .utf8) {
                     // do your stuff here
                      multipart.append(data, withName: key)
                      //print(String(data: data, encoding: String.Encoding.utf8) as Any)
                  }
                              }
            multipart.append(profileimage, withName: "driver_profile", fileName: "profile", mimeType: "image/png")
            multipart.append(licenceimage, withName: "driver_license", fileName: "licence", mimeType: "image/png")

        }
        
        DispatchQueue.main.async
        {

        
            AF.upload(multipartFormData: block, to: url)
                .uploadProgress(queue: .main, closure: { progress in
                    //Current upload progress of file
                    print("Upload Progress: \(progress.fractionCompleted)")
                })
                .response{
                    response in
                    guard let data = response.data else { return }
                    do {
                        let decoder = JSONDecoder()
                        
                        let loginBaseResponse = try? decoder.decode(EmailJson.self, from: data)
                        
                        
                        let code_str = loginBaseResponse?.code
                        print(String(data: data, encoding: String.Encoding.utf8) as Any)
                        
                        DispatchQueue.main.async { [self] in
                            
                            if code_str == 200 {
                                print("success")
                                let defaults = UserDefaults.standard
                                defaults.setValue(loginBaseResponse?.driver?.driverIDPrimary ?? "", forKey: "id") // this is the driver id used
                                defaults.setValue(loginBaseResponse?.driver?.driverName ?? "", forKey: "name")
                                defaults.setValue(loginBaseResponse?.driver?.driverLicenseFile ?? "", forKey: "licenceimg")
                                defaults.setValue(loginBaseResponse?.driver?.profileImage ?? "", forKey: "img")

                                defaults.setValue(loginBaseResponse?.driver?.driverAddress ?? "", forKey: "address")
                                defaults.setValue(loginBaseResponse?.driver?.driverMobile ?? "", forKey: "mobile")
                                let alert = UIAlertController(title: "Profile", message: "Success", preferredStyle: UIAlertController.Style.alert)
                                
                                // add an action (button)
                                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { (_) in
                                    self.navigationController?.popViewController(animated: true)
                                }))
                                
                                // show the alert
                                self.present(alert, animated: true, completion: nil)
                                
                            }
                            else
                            {
                                let alert = UIAlertController(title: "Profile", message: "\(loginBaseResponse?.response ?? "error")", preferredStyle: UIAlertController.Style.alert)
                                
                                // add an action (button)
                                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                                
                                // show the alert
                                self.present(alert, animated: true, completion: nil)
                            }
                            
                        }
                        
                        
                    }
                }
        }
        
        
    }

    func uploadProfileNoImage(to url: URL, params: [String: Any]) {
        let block = { (multipart: MultipartFormData) in
            URLEncoding.default.queryParameters(params).forEach { (key, value) in
                /*  if let data = value.data(using: .utf8) {
                      multipart.append(data, withName: key)
                      print(String(data: data, encoding: String.Encoding.utf8) as Any)

                  } */
                  if let data = value.removingPercentEncoding?.data(using: .utf8) {
                     // do your stuff here
                      multipart.append(data, withName: key)
                      //print(String(data: data, encoding: String.Encoding.utf8) as Any)
                  }
                              }
            //multipart.append(image, withName: "userfile", fileName: "userfile", mimeType: "image/png")
        }
        
        DispatchQueue.main.async
        {

        
            AF.upload(multipartFormData: block, to: url)
                .uploadProgress(queue: .main, closure: { progress in
                    //Current upload progress of file
                    print("Upload Progress: \(progress.fractionCompleted)")
                })
                .response{
                    response in
                    guard let data = response.data else { return }
                    do {
                        let decoder = JSONDecoder()
                        
                        let loginBaseResponse = try? decoder.decode(EmailJson.self, from: data)
                        
                        
                        let code_str = loginBaseResponse?.code
                        print(String(data: data, encoding: String.Encoding.utf8) as Any)
                        
                        DispatchQueue.main.async { [self] in
                            
                            if code_str == 200 {
                                print("success")
                                let defaults = UserDefaults.standard
                                defaults.setValue(loginBaseResponse?.driver?.driverIDPrimary ?? "", forKey: "id") // this is the driver id used
                                defaults.setValue(loginBaseResponse?.driver?.driverName ?? "", forKey: "name")
                                defaults.setValue(loginBaseResponse?.driver?.driverLicenseFile ?? "", forKey: "licenceimg")
                                defaults.setValue(loginBaseResponse?.driver?.profileImage ?? "", forKey: "img")

                                defaults.setValue(loginBaseResponse?.driver?.driverAddress ?? "", forKey: "address")
                                defaults.setValue(loginBaseResponse?.driver?.driverMobile ?? "", forKey: "mobile")
                                let alert = UIAlertController(title: "Profile", message: "Success", preferredStyle: UIAlertController.Style.alert)
                                
                                // add an action (button)
                                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { (_) in
                                    self.navigationController?.popViewController(animated: true)
                                }))
                                
                                // show the alert
                                self.present(alert, animated: true, completion: nil)
                                
                            }
                            else
                            {
                                let alert = UIAlertController(title: "Profile", message: "\(loginBaseResponse?.response ?? "error")", preferredStyle: UIAlertController.Style.alert)
                                
                                // add an action (button)
                                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                                
                                // show the alert
                                self.present(alert, animated: true, completion: nil)
                            }
                            
                        }
                        
                        
                    }
                }
        }
        
        
    }

   

}

extension ProfileUpdate : UINavigationControllerDelegate , UIImagePickerControllerDelegate
{
    func openCamera()
    {
        
        let imagePicker = UIImagePickerController()
        
        imagePicker.sourceType = .camera
        imagePicker.delegate = self
        imagePicker.allowsEditing = true
        present(imagePicker, animated: true)
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true)
        guard let pickedImage = info[.editedImage] as? UIImage else {
            // imageViewPic.contentMode = .scaleToFill
            print("No image found")
            return
        }
        //journalImageView.image = pickedImage
        saveImage(image: pickedImage)
    }
    func openGallery()
    {
        let imagePicker = UIImagePickerController()
        imagePicker.allowsEditing = true

        imagePicker.sourceType = .photoLibrary
        imagePicker.delegate = self
        present(imagePicker, animated: true, completion: nil)
    }
    func saveImage(image : UIImage)
    {                let imageData = image.jpegData(compressionQuality: 0.50)
       // let id = UserDefaults.standard.string(forKey: userDefaultsKey.userMemberID)!
        //let json: [String: Any] = ["user_memberid" : "\(id)"]
        //profileImage = image
       // expenseImage = image
        //expenseImageView.image = image
        //expenseImageView.contentMode = .scaleAspectFit
        
        //profileImageView.cropAsCircleWithBorder(borderColor: .white, strokeWidth: 0)
        //uploadImage(image: imageData!, to: URL(string: "http://thefollo.com/housing/housing_android_api/Imagehelper/users_profileupload")!, params: json)
        switch currentImageViewSelected {
        case 1:
            profile = image
            profileImageView.image = image
        case 2:
            licence = image
            licenceImageView.image = image
        default:
            print("err")
        }
    }
}

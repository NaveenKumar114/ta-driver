//
//  AddExpense.swift
//  Ta Driver
//
//  Created by Naveen Natrajan on 2021-04-28.
//

import UIKit
import Alamofire
class AddExpense: UIViewController, UITextFieldDelegate {
    var jobData : Datum?
    var textfieldPickers = [UITextField]()
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    var editData : Expense?
    var delegate : addExpenseProtocol?
    @IBOutlet weak var amountTextfield: UITextField!
    @IBOutlet weak var modeTextField: UITextField!
    @IBOutlet weak var typeTextField: UITextField!
    @IBOutlet weak var expenseImageView: UIImageView!
    var expenseImage : UIImage?
    var modeEntries = ["CASH" , "CARD" , "ONLINE"]
    var typeEntries = ["FUEL" , "TOUCH N GO" , "FORKLIFT" , "LEVY" , "AUTO PASS" , "TRANSPORT" , "HOTEL" , "FOOD"]
    override func viewDidLoad() {
        super.viewDidLoad()
        amountTextfield.delegate = self
        amountTextfield.tag = 100
        textfieldPickers = [modeTextField , typeTextField]
        for textField in 0 ... textfieldPickers.count - 1
        {
            textfieldPickers[textField].delegate = self
            createPickerView(textField: textfieldPickers[textField], tag: textField)
            //textfieldPickers[textField].font = UIFont(name: "Arial-Rounded", size: 15.0)

        }
        dismissPickerView()
        let gestureident = UITapGestureRecognizer(target: self, action: #selector(toImage))
        expenseImageView.isUserInteractionEnabled = true
        expenseImageView.addGestureRecognizer(gestureident)
        // Do any additional setup after loading the view.
        if editData != nil
        {
            typeTextField.text = editData?.typeofexpense ?? ""
            amountTextfield.text = editData?.expenseamount ?? ""
            modeTextField.text = editData?.modeofpayment ?? ""
            if editData?.attachement1 != nil
            {
                if editData?.attachement1 != ""
                {
                let urlStr = ("\(ConstantsUsedInProject.baseImgUrl)expense/\(editData!.attachement1!)")
                print(urlStr)
                let url = URL(string: urlStr)
                
                DispatchQueue.global().async {
                    let data = try? Data(contentsOf: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
                    DispatchQueue.main.async { [self] in
                        expenseImageView.image = UIImage(data: data!)
                       // logoImageVIew.image = UIImage(data: data!)
                      //  logoImageVIew.contentMode = .scaleAspectFit
                        
                    }
                      
                    }
                }
            }

        }
    }
    @objc func toImage()
    {
        let chooseImageActionMenu = UIAlertController(title: "Choose an option", message: nil, preferredStyle: .actionSheet)
        let galleryButton = UIAlertAction(title: "Gallery", style: .default) { (_) in
            self.openGallery()
        }
        let cameraButton = UIAlertAction(title: "Camera", style: .default) { (_) in
            self.openCamera()
        }
        let cancelButton = UIAlertAction(title: "Cancel", style: .cancel)
        chooseImageActionMenu.addAction(galleryButton)
        chooseImageActionMenu.addAction(cameraButton)
        chooseImageActionMenu.addAction(cancelButton)
        self.present(chooseImageActionMenu, animated: true, completion: nil)
    }

    @IBAction func saveButtonPressed(_ sender: Any) {
        if amountTextfield.text == ""
        {
            let alert = UIAlertController(title: "Expense", message: "Please enter all the details", preferredStyle: UIAlertController.Style.alert)
            
            // add an action (button)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            
            // show the alert
            self.present(alert, animated: true, completion: nil)
        }
        else
        {
            
            let date = NSDate()
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        //    formatter.timeZone = TimeZone(abbreviation: "UTC")
            let defaultTimeZoneStr = formatter.string(from: date as Date)

            let x = jobData!
            let defaults = UserDefaults.standard
            let name = defaults.string(forKey: "name")
            let id = defaults.string(forKey: "id")
            if editData != nil
            {
                let json: [String: Any] = ["expenseid" : "\(editData!.expenseid!)" , "jobid" : "\(x.jobID!)" , "userid" : "\(id!)" , "jobtruckid" : "\(x.truckNumberid!)" , "driverid" : "\(x.taDriverID!)" , "drivername" : "\(x.taDriverName!)", "username" : "\(name!)","description" : "",
                                           "expenseamount" : "\(amountTextfield.text!)" , "typeofexpense" : "\(typeTextField.text!)" , "modeofpayment" : "\(modeTextField.text!)" , "status" : "active" , "createdby" : "\(x.createby!)" , "modifyby" : "\(name!)" ,
                                           "modifydate" : "\(defaultTimeZoneStr)" , "deviceid" : "" , "charge_type" : "\(x.jobChargeType!)","charge_type_id" : "\(x)" , "truck_number" : "\(x.truckNumber!)", "truck_number_id" : "\(x.truckNumberid!)" , "driver_vendor_id" : "" , "driver_vendor_type" : ""]
                if expenseImage != nil
                {
                let imageData = expenseImage!.jpegData(compressionQuality: 0.50)

                uploadImageExpenses(image: imageData!, to: URL(string: "\(ConstantsUsedInProject.baseUrl)job/addexpense")!, params: json)
                }
                else
                {
                    expensesNoImage(to: URL(string: "\(ConstantsUsedInProject.baseUrl)job/addexpense")!, params: json)
                }

            }
            else
            {
            let json: [String: Any] = ["jobid" : "\(x.jobID!)" , "userid" : "\(id!)" , "jobtruckid" : "\(x.truckNumberid!)" , "driverid" : "\(x.taDriverID!)" , "drivername" : "\(x.taDriverName!)", "username" : "\(name!)","description" : "",
                                       "expenseamount" : "\(amountTextfield.text!)" , "typeofexpense" : "\(typeTextField.text!)" , "modeofpayment" : "\(modeTextField.text!)" , "status" : "active" , "createdby" : "\(x.createby!)" , "modifyby" : "\(name!)" ,
                                       "modifydate" : "\(defaultTimeZoneStr)" , "deviceid" : "" , "charge_type" : "\(x.jobChargeType!)","charge_type_id" : "\(x)" , "truck_number" : "\(x.truckNumber!)", "truck_number_id" : "\(x.truckNumberid!)" , "driver_vendor_id" : "" , "driver_vendor_type" : ""]
            if expenseImage != nil
            {
            let imageData = expenseImage!.jpegData(compressionQuality: 0.50)

            uploadImageExpenses(image: imageData!, to: URL(string: "\(ConstantsUsedInProject.baseUrl)job/addexpense")!, params: json)
            }
            else
            {
                expensesNoImage(to: URL(string: "\(ConstantsUsedInProject.baseUrl)job/addexpense")!, params: json)
            }
            }
        }
    }
    func uploadImageExpenses(image: Data, to url: URL, params: [String: Any]) {
        let block = { (multipart: MultipartFormData) in
            URLEncoding.default.queryParameters(params).forEach { (key, value) in
                /*  if let data = value.data(using: .utf8) {
                      multipart.append(data, withName: key)
                      print(String(data: data, encoding: String.Encoding.utf8) as Any)

                  } */
                  if let data = value.removingPercentEncoding?.data(using: .utf8) {
                     // do your stuff here
                      multipart.append(data, withName: key)
                      //print(String(data: data, encoding: String.Encoding.utf8) as Any)
                  }
                              }
            multipart.append(image, withName: "userfile", fileName: "userfile", mimeType: "image/png")
        }
        
        DispatchQueue.main.async
        {

        
            AF.upload(multipartFormData: block, to: url)
                .uploadProgress(queue: .main, closure: { progress in
                    //Current upload progress of file
                    print("Upload Progress: \(progress.fractionCompleted)")
                })
                .response{
                    response in
                    guard let data = response.data else { return }
                    do {
                        let decoder = JSONDecoder()
                        
                        let loginBaseResponse = try? decoder.decode(AddExpensesJSON.self, from: data)
                        
                        
                        let code_str = loginBaseResponse?.header?.status
                        print(String(data: data, encoding: String.Encoding.utf8))
                        
                        DispatchQueue.main.async { [self] in
                            
                                let alert = UIAlertController(title: "Expense", message: "Success", preferredStyle: UIAlertController.Style.alert)
                                delegate?.refresh()
                                // add an action (button)
                                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { (_) in
                                    self.navigationController?.popViewController(animated: true)
                                }))
                                
                                // show the alert
                                self.present(alert, animated: true, completion: nil)
                                
                            
                            
                        }

                        
                        
                    }
                }
        }
        
        
    }
    func expensesNoImage(to url: URL, params: [String: Any]) {
        let block = { (multipart: MultipartFormData) in
            URLEncoding.default.queryParameters(params).forEach { (key, value) in
                /*  if let data = value.data(using: .utf8) {
                      multipart.append(data, withName: key)
                      print(String(data: data, encoding: String.Encoding.utf8) as Any)

                  } */
                  if let data = value.removingPercentEncoding?.data(using: .utf8) {
                     // do your stuff here
                      multipart.append(data, withName: key)
                      //print(String(data: data, encoding: String.Encoding.utf8) as Any)
                  }
                              }
            //multipart.append(image, withName: "userfile", fileName: "userfile", mimeType: "image/png")
        }
        
        DispatchQueue.main.async
        {

        
            AF.upload(multipartFormData: block, to: url)
                .uploadProgress(queue: .main, closure: { progress in
                    //Current upload progress of file
                    print("Upload Progress: \(progress.fractionCompleted)")
                })
                .response{
                    response in
                    guard let data = response.data else { return }
                    do {
                        let decoder = JSONDecoder()
                        
                        let loginBaseResponse = try? decoder.decode(AddExpensesJSON.self, from: data)
                        
                        
                        //let code_str = loginBaseResponse?.header?.status
                        print(String(data: data, encoding: String.Encoding.utf8))
                        
                        DispatchQueue.main.async { [self] in
                            
                                let alert = UIAlertController(title: "Expense", message: "Success", preferredStyle: UIAlertController.Style.alert)
                                delegate?.refresh()
                                // add an action (button)
                                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { (_) in
                                    self.navigationController?.popViewController(animated: true)
                                }))
                                
                                // show the alert
                                self.present(alert, animated: true, completion: nil)
                                
                            
                            
                        }
                        
                        
                    }
                }
        }
        
        
    }

    func createPickerView(textField : UITextField , tag : Int) {
        let pickerView = UIPickerView()
        pickerView.delegate = self
        textField.inputView = pickerView
        textField.tag = tag
        textField.tintColor = UIColor.clear
        pickerView.tag = tag
    }
    func dismissPickerView() {
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        let button = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(self.action))
        toolBar.setItems([button], animated: true)
        toolBar.isUserInteractionEnabled = true
        for textField in textfieldPickers
        {
            textField.inputAccessoryView = toolBar
        }
        
    }
    
    @objc func action() {
        view.endEditing(true)
    }
    

}

extension AddExpense : UIPickerViewDelegate , UIPickerViewDataSource
{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField.text == ""
        {

        switch textField.tag {
        case 0:
            textField.text = modeEntries[0]
        case 1:
            textField.text = typeEntries[0]
        default:
            print("err")
        }
        }
    }
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch pickerView.tag {
        case 0:
            return modeEntries.count
        case 1:
            return typeEntries.count
        case 2:
            return 2
        case 3:
            return 3
        default:
            return 0
        }
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        switch pickerView.tag {
        case 0:
            return modeEntries[row]
        case 1:
            return typeEntries[row]
        default:
            return ""
        }
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        switch pickerView.tag {
        case 0:
            textfieldPickers[0].text = modeEntries[row]
        case 1:
            textfieldPickers[1].text = typeEntries[row]
            
            self.view.endEditing(true)

        default:
            print("err")
            print(pickerView.tag)
        }
    }
}

extension AddExpense : UINavigationControllerDelegate , UIImagePickerControllerDelegate
{
    func openCamera()
    {
        
        let imagePicker = UIImagePickerController()
        
        imagePicker.sourceType = .camera
        imagePicker.delegate = self
        imagePicker.allowsEditing = true
        present(imagePicker, animated: true)
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true)
        guard let pickedImage = info[.editedImage] as? UIImage else {
            // imageViewPic.contentMode = .scaleToFill
            print("No image found")
            return
        }
        //journalImageView.image = pickedImage
        saveImage(image: pickedImage)
    }
    func openGallery()
    {
        let imagePicker = UIImagePickerController()
        imagePicker.allowsEditing = true

        imagePicker.sourceType = .photoLibrary
        imagePicker.delegate = self
        present(imagePicker, animated: true, completion: nil)
    }
    func saveImage(image : UIImage)
    {                let imageData = image.jpegData(compressionQuality: 0.50)
       // let id = UserDefaults.standard.string(forKey: userDefaultsKey.userMemberID)!
        //let json: [String: Any] = ["user_memberid" : "\(id)"]
        //profileImage = image
        expenseImage = image
        expenseImageView.image = image
        expenseImageView.contentMode = .scaleAspectFit
        
        //profileImageView.cropAsCircleWithBorder(borderColor: .white, strokeWidth: 0)
        //uploadImage(image: imageData!, to: URL(string: "http://thefollo.com/housing/housing_android_api/Imagehelper/users_profileupload")!, params: json)
        
    }
}

protocol addExpenseProtocol {
    func refresh()
}

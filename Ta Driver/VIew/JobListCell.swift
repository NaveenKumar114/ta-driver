//
//  JobListCell.swift
//  Ta Driver
//
//  Created by Naveen Natrajan on 2021-04-26.
//

import UIKit

class JobListCell: UITableViewCell {

    @IBOutlet weak var status: UILabel!
    @IBOutlet weak var circleVIew: UIView!
    @IBOutlet weak var address: UILabel!
    @IBOutlet weak var time: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var iconImage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
 
}

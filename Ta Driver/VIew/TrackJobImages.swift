//
//  TrackJobImages.swift
//  Ta Driver
//
//  Created by Naveen Natrajan on 2021-04-28.
//

import UIKit

class TrackJobImages: UITableViewCell {

    @IBOutlet weak var signature: UIImageView!
    @IBOutlet weak var PICKUP: UIImageView!
    @IBOutlet weak var delivery: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var id: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

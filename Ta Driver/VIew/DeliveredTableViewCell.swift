//
//  DeliveredTableViewCell.swift
//  Ta Driver
//
//  Created by Naveen Natrajan on 2021-05-19.
//

import UIKit

class DeliveredTableViewCell: UITableViewCell {

    @IBOutlet weak var viewWithImage: UIView!
    @IBOutlet weak var fromLocation: UILabel!
    @IBOutlet weak var tolocation: UILabel!
    @IBOutlet weak var edpenseBUtton: UIButton!
    
    @IBOutlet weak var taNumber: UILabel!
    @IBOutlet weak var trackButton: UIButton!
    @IBOutlet weak var buttonView: UIView!
    @IBOutlet weak var locImage: UIImageView!
    @IBOutlet weak var status: UILabel!
    @IBOutlet weak var buttonLabel: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var overnight: UILabel!
    @IBOutlet weak var acceptButton: UIButton!
    @IBOutlet weak var to: UILabel!
    @IBOutlet weak var jobNuber: UILabel!
    @IBOutlet weak var from: UILabel!
    @IBOutlet weak var commission: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        viewWithImage.layer.borderWidth = 1
        viewWithImage.layer.borderColor = UIColor.white.cgColor
        viewWithImage.layer.cornerRadius = viewWithImage.frame.height / 2
        buttonView.layer.cornerRadius = 5
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

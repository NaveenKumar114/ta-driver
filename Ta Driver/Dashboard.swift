//
//  Dashboard.swift
//  Ta Driver
//
//  Created by Naveen Natrajan on 2021-04-19.
//
import SystemConfiguration
import QuartzCore
import UIKit
import CoreLocation
import Alamofire
import CoreTelephony
import UIKit
import iCarousel
import CoreLocation
class Dashboard: UIViewController, iCarouselDelegate, iCarouselDataSource , CLLocationManagerDelegate, jobFinishedDelegate , deliveredJobsProtocol {
    func toExpenses(data: Datum) {
        delivereJObDatum = data
        performSegue(withIdentifier: "toExpense", sender: nil)
        

    }
    
    func addExpense(data: Datum) {
        delivereJObDatum = data
        performSegue(withIdentifier: "deliverToAddExpense", sender: nil)
        delivereJObDatum = data
    }
    
    func toTrack(data: Datum) {
        delivereJObDatum = data
        performSegue(withIdentifier: "toTrack", sender: nil)
        delivereJObDatum = data
    }
    
    func refresh() {
        makePostCallAcceptd()
    }
    
    func jobDelivered() {
        t.suspend()
    }
    @IBOutlet weak var dashBoardTable: UITableView!
    var delivereJObDatum : Datum?
    @IBOutlet weak var deliveredContainer: UIView!
    var assignedJobData : AssignedJobJSON?
    @IBOutlet weak var carouselView: iCarousel!
    var selectedType : jobType = .all
    var selectedJob = 0
    var time = 3
    var flag = 0
    var kilFlag = 0
    @IBOutlet weak var dashSegment: UISegmentedControl!
    var locationManager: CLLocationManager?

    var currentLocation : CLLocation?
    var previousLocation : CLLocation?

    var timer = Timer()
    let t = RepeatingTimer(timeInterval: 10)

    override func viewDidLoad() {
        super.viewDidLoad()
        carouselView.delegate = self
        carouselView.dataSource = self
        carouselView.type = .rotary
        carouselView.reloadData()
        makePostCallAll()
        UserDefaults.standard.setValue("AALEN", forKey: userdefaultsKey.createBy)
        let name = UserDefaults.standard.string(forKey: "name")
        self.title = name
        t.resume()
        print(UserDefaults.standard.string(forKey: userdefaultsKey.jobSelected))
        if UserDefaults.standard.string(forKey: userdefaultsKey.jobSelected) == "true"
        {
            t.resume()
        }
        else
        {
            t.suspend()
        }
        t.eventHandler = { [self] in
            print("Timer Fired")
            time = time + 3
            print(Date())
            flag = 1
            locationManager?.requestLocation()
            print(UIApplication.shared.backgroundTimeRemaining)
        
            
        }
      //  t.suspend()
        locationManager = CLLocationManager()
        locationManager?.delegate = self
        locationManager?.requestAlwaysAuthorization()
        locationManager?.allowsBackgroundLocationUpdates = true
       // locationManager?.requestLocation()
      //  locationManager?.startUpdatingLocation()
        
        
        dashBoardTable.delegate = self
        dashBoardTable.dataSource = self
        dashBoardTable.register(UINib(nibName: "DashboardTableCell", bundle: nil) , forCellReuseIdentifier: "DashboardTableCell")
        let i = #imageLiteral(resourceName: "expense_forklift")
     //   dashSegment.setImage(UIImage.textEmbeded(image: i, string: "dd", isImageBeforeText: true), forSegmentAt: 0)
        dashSegment.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.white], for: .selected)
        dashSegment.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: ConstantsUsedInProject.appThemeColor], for: .normal)


        
    }
    func convertDateFormater(_ date: String) -> String
        {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            let date = dateFormatter.date(from: date)
            dateFormatter.dateFormat = "dd-MM-yyyy"
            return  dateFormatter.string(from: date!)

        }
    func getConnectionType() -> String {
        guard let reachability = SCNetworkReachabilityCreateWithName(kCFAllocatorDefault, "www.google.com") else {
            return "NO INTERNET"
        }
        
        var flags = SCNetworkReachabilityFlags()
        SCNetworkReachabilityGetFlags(reachability, &flags)
        
        let isReachable = flags.contains(.reachable)
        let isWWAN = flags.contains(.isWWAN)
        
        if isReachable {
            if isWWAN {
                let networkInfo = CTTelephonyNetworkInfo()
                let carrierType = networkInfo.serviceCurrentRadioAccessTechnology
                
                guard let carrierTypeName = carrierType?.first?.value else {
                    return "UNKNOWN"
                }
                
                switch carrierTypeName {
                case CTRadioAccessTechnologyGPRS, CTRadioAccessTechnologyEdge, CTRadioAccessTechnologyCDMA1x:
                    return "2G"
                case CTRadioAccessTechnologyLTE:
                    return "4G"
                default:
                    return "3G"
                }
            } else {
                return "WIFI"
            }
        } else {
            return "NO INTERNET"
        }
    }
    func degreesToRadians(degrees: Double) -> Double { return degrees * .pi / 180.0 }
    func radiansToDegrees(radians: Double) -> Double { return radians * 180.0 / .pi }

    func getBearingBetweenTwoPoints1(point1 : CLLocation, point2 : CLLocation) -> (Double , Double) {

        let lat1 = degreesToRadians(degrees: point1.coordinate.latitude)
        let lon1 = degreesToRadians(degrees: point1.coordinate.longitude)

        let lat2 = degreesToRadians(degrees: point2.coordinate.latitude)
        let lon2 = degreesToRadians(degrees: point2.coordinate.longitude)

        let dLon = lon2 - lon1

        let y = sin(dLon) * cos(lat2)
        let x = cos(lat1) * sin(lat2) - sin(lat1) * cos(lat2) * cos(dLon)
        let radiansBearing = atan2(y, x)
        //print(radiansBearing)
        return (radiansToDegrees(radians: radiansBearing) , radiansBearing)
    }
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error)
    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if flag == 1
        {
            //flag = 0
            if UIApplication.shared.applicationState  == .active
            {
                print("active")
                if let location = locations.last {
                    if currentLocation == nil
                    {
                        currentLocation = location
                        previousLocation = currentLocation
                    }
                    else
                    {
                        previousLocation = currentLocation
                        currentLocation = location
                        
                    }
                    let distance = currentLocation?.distance(from: previousLocation!)
                    
                    print(Double(distance!) / 1000)
                    print("New speed is \(location.speed)")
                    print("New latitude is \(location.coordinate.latitude)")
                    print("New long is \(location.coordinate.longitude)")
                    prepareForUpload()
                }
            }
            if UIApplication.shared.applicationState == .active {
                print("da")
            } else {
                print("App is backgrounded. New location is %@", locations.last as Any)
                print(locations.first as Any)
                if let location = locations.last {
                    if currentLocation == nil
                    {
                        currentLocation = location
                        previousLocation = currentLocation
                    }
                    else
                    {
                        previousLocation = currentLocation
                        currentLocation = location
                        
                    }
                    let distance = currentLocation?.distance(from: previousLocation!)
                    
                    print("New speed is \(location.speed)")
                    print("New latitude is \(location.coordinate.latitude)")
                    print("New long is \(location.coordinate.longitude)")
               //     getAddressFromLatLon(pdblLatitude: String(location.coordinate.latitude), withLongitude: String(location.coordinate.longitude) , speed: String(location.speed) , accuracy: location.horizontalAccuracy, distance: String(distance!))
                }
            }
            
        }
        
    }
    
    @IBAction func logoutPressed(_ sender: Any) {
      
        
        let alert = UIAlertController(title: "LogOut", message: "Are You Sure You Want To Sign Out", preferredStyle: .alert)
        
        let ok = UIAlertAction(title: "OK", style: .default, handler: { [self] action in
            let defaults = UserDefaults.standard
            defaults.setValue("false", forKey: "\(userdefaultsKey.jobSelected)")
            t.suspend()
            defaults.setValue("FALSE", forKey: "loggedIn")

            let storyboard = UIStoryboard(name: "Login", bundle: nil)

            let dashboard1 = storyboard.instantiateViewController(identifier: "login")
            (UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate)?.changeRootViewController(dashboard1)
        })
        alert.addAction(ok)
        let cancel = UIAlertAction(title: "Cancel", style: .default, handler: { action in
        })
        alert.addAction(cancel)
        DispatchQueue.main.async(execute: {
            self.present(alert, animated: true)
        })

    }
    func prepareForUpload()
    {
        UIDevice.current.isBatteryMonitoringEnabled = true
        print(UIDevice.current.isBatteryMonitoringEnabled)
        var level = UIDevice.current.batteryLevel
        print(level*100)
        if level == -1.0
        {
            level = 0.00
        }
        let version = UIDevice.current.systemVersion
        print(version)
        let name = UIDevice.current.name
        print(name)
        let model = UIDevice.current.model
        print(model)
        let state = UIDevice.current.batteryState
        var batteryType = "discharging"
        switch state {
        case .charging:
            print("charging")
        case .full:
            batteryType = "full"
            
        case .unplugged:
            batteryType = "discharging"
            
        default:
            print("else")
        }
        print(getConnectionType())
        let networkInfo = CTTelephonyNetworkInfo()
        let carrier = networkInfo.subscriberCellularProvider
        
        // Get carrier name
        var carrierName = carrier?.carrierName
        if carrierName == nil
        {
            carrierName = ""
        }
       
        let id = UserDefaults.standard.string(forKey: "id")
        print("hhh")
        //let x = jobData!
        var lat = ""
        var long = ""
        var speed = ""
        var speedAccur = ""
        var accur = ""
        var bearing = ""
        var bearingDegree = ""
        if currentLocation != nil{
            lat = String((currentLocation?.coordinate.latitude)!)
            long = String((currentLocation?.coordinate.longitude)!)
            speed = String((currentLocation?.speed)!)
            speedAccur = String((currentLocation?.speedAccuracy)!)
            accur = String((currentLocation?.horizontalAccuracy)!)
            if previousLocation != nil
            {
                let y = getBearingBetweenTwoPoints1(point1: currentLocation!, point2: previousLocation!)
                bearingDegree = String(y.0)
                bearing = String(y.1)
                print(bearing)
                
            }

        }
        let date = NSDate()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
    //    formatter.timeZone = TimeZone(abbreviation: "UTC")
        let defaultTimeZoneStr = formatter.string(from: date as Date)
   
        let defaults = UserDefaults.standard
        
        let json: [String: Any] = ["sign_emp_id" : "","sign_emp_name" : "" , "trackid" : "","jobid" : "\(defaults.string(forKey: userdefaultsKey.jobid)!)" , "userid" : "\(defaults.string(forKey: userdefaultsKey.userID)!)" , "jobtruckid" : "\(defaults.string(forKey: userdefaultsKey.truckid)!)" , "clientid" : "\(defaults.string(forKey: userdefaultsKey.cliendid)!)" , "driverid" : "\(defaults.string(forKey: userdefaultsKey.driverID)!)" , "jobstatus" : "\(defaults.string(forKey: userdefaultsKey.jobStatus)!)" , "description" : "" , "trackdatetime" : "\(defaultTimeZoneStr)" , "currentlocation" : "\(lat),\(long)" ,"destinationlocation" : "0,0" , "pickuplocation" : "0,0" , "latitude" : "\(lat)" , "longitude" : "\(long)" , "status" : "active" , "createby" : "\(defaults.string(forKey: userdefaultsKey.createBy)!)", "modifyby" : "\(defaults.string(forKey: userdefaultsKey.name)!)" , "modifydate" : "\(defaultTimeZoneStr)" ,"deviceid" : "" , "bearing" : "\(bearing)" , "bearingaccuracydegrees" : "\(bearingDegree)" , "speed" : "\(speed)" , "speedaccuracymeterspersecond" : "\(speedAccur)" , "accuracy" : "\(accur)" , "jobnumber" : "\(defaults.string(forKey: userdefaultsKey.jobNumber)!)" , "battery_type" : "\(batteryType)" , "battery_percentage" : "\(Int(level*100))" , "network_type" : "\(getConnectionType())" , "network_operator" : "\(carrierName ?? "")" , "geofence" : "EXIT Warehouse" , "support_driver_flag" : ""]
         //   let imageData = fromAttachImage!.jpegData(compressionQuality: 0.50)

            uploadImage(to: URL(string: "\(ConstantsUsedInProject.baseUrl)api/addtrackwithflag")!, params: json)
        
     

    }
    func uploadImage( to url: URL, params: [String: Any]) {
        let block = { (multipart: MultipartFormData) in
            URLEncoding.default.queryParameters(params).forEach { (key, value) in
                /*  if let data = value.data(using: .utf8) {
                      multipart.append(data, withName: key)
                      print(String(data: data, encoding: String.Encoding.utf8) as Any)

                  } */
                  if let data = value.removingPercentEncoding?.data(using: .utf8) {
                     // do your stuff here
                      multipart.append(data, withName: key)
                      //print(String(data: data, encoding: String.Encoding.utf8) as Any)
                  }
                              }

            //multipart.append(image, withName: "attachmentimage", fileName: "profile", mimeType: "image/png")
        }
        
        DispatchQueue.main.async
        {

        
            AF.upload(multipartFormData: block, to: url)
                .uploadProgress(queue: .main, closure: { progress in
                    //Current upload progress of file
                    print("Upload Progress: \(progress.fractionCompleted)")
                })
                .response{
                    response in
                    guard let data = response.data else { return }
                    do {
                        let decoder = JSONDecoder()
                        
                        let loginBaseResponse = try? decoder.decode(JobDepartureDeliveryJSON.self, from: data)
                        
                        
                        let code_str = loginBaseResponse?.code
                        print(String(data: data, encoding: String.Encoding.utf8))
                        
                        DispatchQueue.main.async { [self] in
                            
                            if code_str == 200 {
                               print("success")
                          
                                
                                
                            }
                            else
                            {
                              
                            }
                            
                        }
                        
                        
                    }
                }
        }
        
        
    }

    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedAlways {
        }
    }
    func numberOfItems(in carousel: iCarousel) -> Int {
        return assignedJobData?.data?.count ?? 0
    }
    
    @IBAction func segmentChnaged(_ sender: UISegmentedControl) {
        if sender.selectedSegmentIndex == 0
        {
            selectedType = .all
            makePostCallAll()
            deliveredContainer.isHidden = true
        }
        else
        if sender.selectedSegmentIndex == 1
        {
            selectedType = .assigned
            makePostCall()
            deliveredContainer.isHidden = true
        }
        
        else
        if sender.selectedSegmentIndex == 2

        {
            selectedType = .accepted
            makePostCallAcceptd()
            deliveredContainer.isHidden = true
        }
        else
        if sender.selectedSegmentIndex == 3

        {
           
            deliveredContainer.isHidden = false
        }
        
    }
    
    func carousel(_ carousel: iCarousel, viewForItemAt index: Int, reusing view: UIView?) -> UIView {
        let frame  = CGRect(x: 0, y: 0, width: 330, height: 400)
    
        let x = assignedJobData?.data?[index]
        let myView = DashboardEntries.instantiate(message: "\(index)")
        myView.frame = frame
        myView.x()
        let y = Double(x?.overnightTotalAmount ?? "0.0")
        let s = String(format: "%.2f", y ?? 0.0)
        myView.advance.text = "MYR \(s)"
        myView.date.text = convertDateFormater((x?.jobDate!)!)
        myView.comission.text = "MYR \(x?.jobCommision ?? "")"
        myView.taNumber.text = x?.taTadynamicNumber
        myView.from.text = x?.jobFrom?.uppercased()
        myView.to.text = x?.jobClientName?.uppercased()
        myView.jobNumbwe.text = x?.jobNumber
        //myView.frame = frame
        myView.acceptButton.tag = index
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector (acceptPressed))  //Tap function will call when user tap on button
        myView.acceptButton.addGestureRecognizer(tapGesture)
        myView.segmentCOntrol.tag = index
        myView.segmentCOntrol.addTarget(self, action: #selector(segmentAction(_:)), for: .valueChanged)
        if selectedType == .accepted
        {
            myView.jobNmae.text = x?.jobStatus?.uppercased()

            myView.acceptButton.setTitle("VIEW JOB", for: .normal)
        }
        else
        {
            myView.jobNmae.text = "ASSIGNED \(index + 1)"

            myView.acceptButton.setTitle("ACCEPT", for: .normal)

        }

        return myView
    }
    @objc func segmentAction(_ segmentedControl: UISegmentedControl) {
        let i = segmentedControl.tag
           switch (segmentedControl.selectedSegmentIndex) {
           case 0:
            let c = carouselView.currentItemView as! DashboardEntries
            c.from.text = assignedJobData?.data?[i].jobFrom?.uppercased()
            print("1")
               break // Uno
           case 1:
            let c = carouselView.currentItemView as! DashboardEntries
            c.from.text = assignedJobData?.data?[i].jobTo?.uppercased()
            print("2")
               break // Dos
           case 2:
               break // Tres
           default:
               break
           }
       }
    @objc func acceptPressed(sender: UITapGestureRecognizer)
    {
        let i = sender.view?.tag
        if selectedType == .all
        {
            let x = assignedJobData?.data?[i!]
            if x?.jobStatus?.uppercased() == "PICKUP ARRANGED"
            {
                print("accept")
                makePostCallAccept(cliendId: x!.taFormID!, jobid: x!.jobID!, truckid: x!.truckNumberid!, dricerId: x!.taDriverID!, jobNumber: x!.jobNumber!, createBY: x!.createby!)
            }
            else
            {
                print(selectedJob)
                selectedJob = i!
                performSegue(withIdentifier: "toJob", sender: nil)
            //cell.buttonLabel.text = "VIEW JOB"
            }

        }
        else
        {
        if selectedType == .assigned
        {
        let x = assignedJobData?.data?[i!]
        print("selceted")
        if selectedType == .assigned {
            makePostCallAccept(cliendId: x!.taFormID!, jobid: x!.jobID!, truckid: x!.truckNumberid!, dricerId: x!.taDriverID!, jobNumber: x!.jobNumber!, createBY: x!.createby!)
            
        }
        }
        else
        {
            print(selectedJob)
            selectedJob = i!
            performSegue(withIdentifier: "toJob", sender: nil)
        }
        }
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toJob"
        {
            let x = assignedJobData?.data?[selectedJob]

            let vc = segue.destination as! JobDetails
            vc.jobData = x
            vc.delegate = self
        }
        if segue.identifier == "delivered"
        {
            let vc = segue.destination as! DeliveredJobs
            vc.delegate = self
        }
        if segue.identifier == "toExpense"
        {
            let x = delivereJObDatum

            let vc = segue.destination as! Expenses
            vc.jobData = x
        }
        if segue.identifier == "toTrack"
        {
            let x = delivereJObDatum
            let vc = segue.destination as! TrackJobDelivered
            vc.jobData = x
        }
        if segue.identifier == "deliverToAddExpense"
        {
            let x = delivereJObDatum
            let vc = segue.destination as! AddExpense
            vc.jobData = x
        }
    }
    func makePostCallAll() {
      
       
        let id = UserDefaults.standard.string(forKey: "id")
        let decoder = JSONDecoder()
       
        let postString = "id=\(id!)&status=ALL"
        // create post request
        if let url = URL(string: "\(ConstantsUsedInProject.baseUrl)job/driverjob")
        {
            let request = NSMutableURLRequest(url: url)
            request.httpMethod = "POST"
            request.setValue("application/x-www-form-urlencoded; charset=utf-8", forHTTPHeaderField: "Content-Type")
            request.httpBody = postString.data(using: String.Encoding.utf8)
            let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
                guard error == nil && data != nil else {                                                          // check for fundamental networking error
                    print("error=\(String(describing: error))")
                    return
                }
                do {
                    
                    let loginBaseResponse = try? decoder.decode(AssignedJobJSON.self, from: data!)
                    let code_str = loginBaseResponse!.code
                    // print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                    
                    DispatchQueue.main.async { [self] in
                        
                        if code_str == 200 {
                            
                          assignedJobData = nil
                            print(loginBaseResponse as Any)
                            assignedJobData = loginBaseResponse
                            carouselView.reloadData()
                            dashBoardTable.reloadData()
                        }else if code_str == 201  {
                            //print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                            
                            let alert = UIAlertController(title: "DriverJob", message: "\(loginBaseResponse?.response ?? "error")", preferredStyle: UIAlertController.Style.alert)
                            
                            // add an action (button)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                            
                            // show the alert
                            self.present(alert, animated: true, completion: nil)
                            
                            
                        }
                        
                    }
                    
                    
                }
            }
            task.resume()
        }
    }
    func makePostCall() {
      
       
        let id = UserDefaults.standard.string(forKey: "id")
        let decoder = JSONDecoder()
       
        let postString = "id=\(id!)&status=ASSIGNED"
        // create post request
        if let url = URL(string: "\(ConstantsUsedInProject.baseUrl)job/driverjob")
        {
            let request = NSMutableURLRequest(url: url)
            request.httpMethod = "POST"
            request.setValue("application/x-www-form-urlencoded; charset=utf-8", forHTTPHeaderField: "Content-Type")
            request.httpBody = postString.data(using: String.Encoding.utf8)
            let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
                guard error == nil && data != nil else {                                                          // check for fundamental networking error
                    print("error=\(String(describing: error))")
                    return
                }
                do {
                    
                    let loginBaseResponse = try? decoder.decode(AssignedJobJSON.self, from: data!)
                    let code_str = loginBaseResponse!.code
                    // print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                    
                    DispatchQueue.main.async { [self] in
                        
                        if code_str == 200 {
                            
                          assignedJobData = nil
                            print(loginBaseResponse as Any)
                            assignedJobData = loginBaseResponse
                            carouselView.reloadData()
                            dashBoardTable.reloadData()
                        }else if code_str == 201  {
                            //print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                            
                            let alert = UIAlertController(title: "DriverJob", message: "\(loginBaseResponse?.response ?? "error")", preferredStyle: UIAlertController.Style.alert)
                            
                            // add an action (button)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                            
                            // show the alert
                            self.present(alert, animated: true, completion: nil)
                            
                            
                        }
                        
                    }
                    
                    
                }
            }
            task.resume()
        }
    }
    func makePostCallAcceptd() {
      
       
        let id = UserDefaults.standard.string(forKey: "id")
        let decoder = JSONDecoder()
       
        let postString = "id=\(id!)&status=ACCEPTED"
        // create post request
        if let url = URL(string: "\(ConstantsUsedInProject.baseUrl)job/driverjob")
        {
            let request = NSMutableURLRequest(url: url)
            request.httpMethod = "POST"
            request.setValue("application/x-www-form-urlencoded; charset=utf-8", forHTTPHeaderField: "Content-Type")
            request.httpBody = postString.data(using: String.Encoding.utf8)
            let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
                guard error == nil && data != nil else {                                                          // check for fundamental networking error
                    print("error=\(String(describing: error))")
                    
                    return
                }
                do {
                    
                    let loginBaseResponse = try? decoder.decode(AssignedJobJSON.self, from: data!)
                    let code_str = loginBaseResponse!.code
                    // print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                    
                    DispatchQueue.main.async { [self] in
                        
                        if code_str == 200 {
                            
                          assignedJobData = nil
                            print(loginBaseResponse as Any)
                            assignedJobData = loginBaseResponse
                            carouselView.reloadData()
                            dashBoardTable.reloadData()
                        }else if code_str == 201  {
                            //print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                            
                            print(loginBaseResponse as Any)

                            let alert = UIAlertController(title: "DriverJob", message: "\(loginBaseResponse?.response ?? "error")", preferredStyle: UIAlertController.Style.alert)
                            
                            // add an action (button)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                            
                            // show the alert
                            self.present(alert, animated: true, completion: nil)
                            
                            
                        }
                        
                    }
                    
                    
                }
            }
            task.resume()
        }
    }
    func makePostCallAccept(cliendId : String , jobid : String , truckid : String , dricerId : String , jobNumber : String , createBY : String) {
      
       
        let id = UserDefaults.standard.string(forKey: "id")
        let decoder = JSONDecoder()
       
        let postString = "clientid=\(cliendId)&status=ACCEPTED&jobid=\(jobid)&truckid=\(truckid)&reason=&driverid=\(dricerId)"
        // create post request
        if let url = URL(string: "\(ConstantsUsedInProject.baseUrl)api/acceptedstatus")
        {
            print(url)
            let request = NSMutableURLRequest(url: url)
            request.httpMethod = "POST"
            request.setValue("application/x-www-form-urlencoded; charset=utf-8", forHTTPHeaderField: "Content-Type")
            request.httpBody = postString.data(using: String.Encoding.utf8)
            let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
                guard error == nil && data != nil else {                                                          // check for fundamental networking error
                    print("error=\(String(describing: error))")
                    
                    return
                }
                do {
                     print(String(data: data!, encoding: String.Encoding.utf8) as Any)

                    let loginBaseResponse = try? decoder.decode(AssignedJobJSON.self, from: data!)
                    let code_str = loginBaseResponse!.code
                    // print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                    
                    DispatchQueue.main.async { [self] in
                        
                        if code_str == 200 {
                            print(loginBaseResponse as Any)

                            let defaults = UserDefaults.standard
                            defaults.setValue("\(cliendId)", forKey: "\(userdefaultsKey.cliendid)")
                            defaults.setValue("\(dricerId)", forKey: "\(userdefaultsKey.driverID)")
                            defaults.setValue("COLLECTING", forKey: "\(userdefaultsKey.jobStatus)")
                            defaults.setValue("\(jobid)", forKey: "\(userdefaultsKey.jobid)")
                            defaults.setValue("\(truckid)", forKey: "\(userdefaultsKey.truckid)")
                            defaults.setValue("\(cliendId)", forKey: "\(userdefaultsKey.userID)")
                            defaults.setValue("\(jobNumber)", forKey: "\(userdefaultsKey.jobNumber)")
                            defaults.setValue("\(createBY)", forKey: "\(userdefaultsKey.createBy)")

                            defaults.setValue("true", forKey: "\(userdefaultsKey.jobSelected)")
                            t.resume()
                            let alert = UIAlertController(title: "Job", message: "\(loginBaseResponse?.response ?? "error")", preferredStyle: UIAlertController.Style.alert)
                            
                            // add an action (button)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { (_) in
                                dashSegment.selectedSegmentIndex = 2
                                selectedType = .accepted
                                makePostCallAcceptd()
                                deliveredContainer.isHidden = true

                            }))
                            
                            // show the alert
                            self.present(alert, animated: true, completion: nil)
                         // assignedJobData = nil
                            print(loginBaseResponse as Any)
                           // carouselView.reloadData()
                        }else if code_str == 201  {
                            //print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                            
                            print(loginBaseResponse as Any)

                            let alert = UIAlertController(title: "Job", message: "\(loginBaseResponse?.response ?? "error")", preferredStyle: UIAlertController.Style.alert)
                            
                            // add an action (button)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                            
                            // show the alert
                            self.present(alert, animated: true, completion: nil)
                            
                            
                        }
                        
                    }
                    
                    
                }
            }
            task.resume()
        }
    }
}

extension Dashboard : UITableViewDelegate , UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return assignedJobData?.data?.count ?? 0
      //  return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = dashBoardTable.dequeueReusableCell(withIdentifier: "DashboardTableCell") as! DashboardTableCell
        let x = assignedJobData?.data?[indexPath.row]
        
        let y = Double(x?.overnightTotalAmount ?? "0.0")
        let s = String(format: "%.2f", y ?? 0.0)
        cell.overnight.text = "MYR \(s)"
        cell.date.text = convertDateFormater((x?.jobDate!)!)
        cell.commission.text = "MYR \(x?.jobCommision ?? "")"
        cell.taNumber.text = x?.taTadynamicNumber
        cell.from.text = x?.jobFrom?.uppercased()
        cell.to.text = x?.jobClientName?.uppercased()
        cell.jobNuber.text = x?.jobNumber
        cell.tolocation.text = x?.billtoLocationaddress
        cell.fromLocation.text = x?.consigneeLocationaddress
        if selectedType == .accepted
        {
            cell.status.text = x?.jobStatus?.uppercased()


            //cell.acceptButton.setTitle("VIEW JOB", for: .normal)
            cell.buttonLabel.text = "VIEW JOB"
        }
        else
        if selectedType == .assigned
            {
            cell.status.text = "ASSIGNED \(indexPath.row + 1)"

            cell.buttonLabel.text = "ACCEPT"
        }
        else
        {
           
            cell.status.text = x?.jobStatus?.uppercased()


            if x?.jobStatus?.uppercased() == "PICKUP ARRANGED"
            {
                cell.buttonLabel.text = "ACCEPT"

            }
            else
            {
            cell.buttonLabel.text = "VIEW JOB"
            }
        }
        cell.acceptButton.tag = indexPath.row
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector (acceptPressed))  //Tap function will call when user tap on button
        cell.acceptButton.addGestureRecognizer(tapGesture)
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 216
    }
}

enum jobType {
    case all
    case assigned
    case accepted
}


extension UIImage {
    static func textEmbeded(image: UIImage,
                           string: String,
                isImageBeforeText: Bool,
                          segFont: UIFont? = nil) -> UIImage {
        let font = segFont ?? UIFont.systemFont(ofSize: 16)
        let expectedTextSize = (string as NSString).size(withAttributes: [.font: font])
        let width = expectedTextSize.width + image.size.width + 5
        let height = max(expectedTextSize.height, image.size.width)
        let size = CGSize(width: width, height: height)

        let renderer = UIGraphicsImageRenderer(size: size)
        return renderer.image { context in
            let fontTopPosition: CGFloat = (height - expectedTextSize.height) / 2
            let textOrigin: CGFloat = isImageBeforeText
                ? image.size.width + 5
                : 0
            let textPoint: CGPoint = CGPoint.init(x: textOrigin, y: fontTopPosition)
            string.draw(at: textPoint, withAttributes: [.font: font])
            let alignment: CGFloat = isImageBeforeText
                ? 0
                : expectedTextSize.width + 5
            let rect = CGRect(x: alignment,
                              y: (height - image.size.height) / 2,
                          width: image.size.width,
                         height: image.size.height)
            image.draw(in: rect)
        }
    }
}

//
//  ViewController.swift
//  Ta Driver
//
//  Created by Naveen Natrajan on 2021-04-17.
//

import UIKit
import iCarousel
class ViewController: UIViewController, iCarouselDataSource, iCarouselDelegate {
    func numberOfItems(in carousel: iCarousel) -> Int {
        return 5
    }
    
    func carousel(_ carousel: iCarousel, viewForItemAt index: Int, reusing view: UIView?) -> UIView {
        let myView = UIView(frame: CGRect(x: 0, y: 0, width: 200, height: 400))
        switch index {
        case 0:
            myView.backgroundColor = .red
        case 1:
            myView.backgroundColor = .green
        case 2:
            myView.backgroundColor = .yellow

        case 3:
            myView.backgroundColor = .purple

        case 4:
            myView.backgroundColor = .blue

        default:
            print("err")
        }
        return myView
    }
    

    @IBOutlet weak var carouselView: iCarousel!
    override func viewDidLoad() {
        super.viewDidLoad()
        carouselView.delegate = self
        carouselView.dataSource = self
        carouselView.type = .rotary
        carouselView.reloadData()
        
        let storyboard2 = UIStoryboard(name: "Dashboard", bundle: nil)

        let dashboard = storyboard2.instantiateViewController(identifier: "dash")

        
        let storyboard = UIStoryboard(name: "Login", bundle: nil)

        let dashboard1 = storyboard.instantiateViewController(identifier: "login")

        
        let loggedUsername = UserDefaults.standard.string(forKey: "loggedIn")
            if loggedUsername == "TRUE" {
                // instantiate the main tab bar controller and set it as root view controller
                // using the storyboard identifier we set earlier
                
                (UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate)?.changeRootViewController(dashboard)
              }
        else
            {
                (UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate)?.changeRootViewController(dashboard1)               }

    }


}

